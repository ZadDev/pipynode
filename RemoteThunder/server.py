__author__ = 'barld'

import cgi,time
import os
import Commands
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer


class MyHandler(BaseHTTPRequestHandler):

    c = Commands.Command()

    def iscommand(self, path=""):
        if path != "/" and path != "":
            if path == "/left.start":
                self.c.start("left")
                return True
            elif path == "/right.start":
                self.c.start("right")
                return True
            elif path == "/up.start":
                self.c.start("up")
                return True
            elif path == "/down.start":
                self.c.start("down")
                return True
            elif path.split(".")[0] == "/fire":
                self.c.fire()
                return True
            elif path.split(".")[1]=="stop":
                self.c.stop()
                return True
            else:
                return False
        else:
            return False


    def do_GET(self):
        if self.iscommand(self.path):
            self.wfile.write("hoi")
            return #return is belangerijk anders crasht de server
        else:
            if os.path.isfile(("WebFiles/"+self.path).split("?")[0]):
                f = open(("WebFiles/"+self.path).split("?")[0])
                self.wfile.write(f.read())
                return


def main():#wordt uitgevoerd zodra de server wordt gestart
    try:
        server = HTTPServer(('', 80), MyHandler)
        print 'server is gestart op poort 80'
        server.serve_forever()
    except KeyboardInterrupt:
        print '^C received, shutting down server'
        server.socket.close()

if __name__ == '__main__':
    main()