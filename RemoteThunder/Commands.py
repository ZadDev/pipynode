import Armageddon

__author__ = 'barld'


class Command:

    a = Armageddon.Armageddon()

    def __init__(self):
        pass

    def start(self, way):
        if way=="left":
            self.a.send_cmd(self.a.LEFT)
        elif way == "right":
            self.a.send_cmd(self.a.RIGHT)
        elif way == "up":
            self.a.send_cmd(self.a.UP)
        elif way == "down":
            self.a.send_cmd(self.a.DOWN)

    def stop(self):
        self.a.send_cmd(self.a.STOP)

    def fire(self, aantal=1):
        self.a.send_cmd(self.a.FIRE)