__author__ = 'barld'

import pifacecad
import  os


cad = pifacecad.PiFaceCAD()

def goleft(event):
    while cad.switches[6].value ==1:
        os.system("sudo python ~/pythonkloter/command.py left 400")


def goright(event):
    while cad.switches[7].value ==1:
        os.system("sudo python ~/pythonkloter/command.py right 400")

def fire(event):
    while cad.switches[5].value ==1:
        os.system("sudo python ~/pythonkloter/command.py fire 1")



listener = pifacecad.SwitchEventListener(chip=cad)

##registreer events
#back
listener.register(6 , pifacecad.IODIR_FALLING_EDGE, goleft)
listener.register(7 , pifacecad.IODIR_FALLING_EDGE, goright)
listener.register(5 , pifacecad.IODIR_FALLING_EDGE, fire)


listener.activate()