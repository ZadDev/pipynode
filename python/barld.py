import pifacecad as p
import urllib.request
import http.client, urllib.parse
import math

def KnopCijfer(event):
    cijfer = int(event.interrupt_flag)
    rtw = 1
    while cijfer > 1:
        cijfer /= 2
        rtw += 1
    print(rtw)
    return rtw

cad = p.PiFaceCAD()
def print_flag(event):
    params = urllib.parse.urlencode({'knop': KnopCijfer(event)})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    conn = http.client.HTTPConnection("127.0.0.1",3000)
    conn.request("POST", "/", params, headers)
    response = conn.getresponse()
    conn.close()
listener = p.SwitchEventListener()
for i in range(len(cad.switches)):
    listener.register(i, p.IODIR_ON, print_flag)

listener.activate()

cad.lcd.blink_off()
cad.lcd.cursor_off()
cad.lcd.backlight_on()
cad.lcd.write("Hallo Zeeland!")