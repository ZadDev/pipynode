
/**
 * Module dependencies.
 */

var express = require('express.io')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , app = express().http().io();

app.use(express.cookieParser());
app.use(express.session({secret: 'monkey'}));
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.post('/', function(req, res) { 
	req.io.route('knopje');
	res.render('send', { color: req.body.knop });
});

app.io.route('knopje', function(req) {
	switch(req.body.knop)
	{
		case '1':
		  	console.log("Groen");
		  	req.io.broadcast('backgroundimage', "green");
		  	break;
		case '2':
		  	console.log("Geel");
		  	req.io.broadcast('backgroundimage', "yellow");
		  	break;
		case '3':
		  	console.log("Blauw");
		  	req.io.broadcast('backgroundimage', "blue");
		  	break;
		case '4':
		  	console.log("Paars");
		  	req.io.broadcast('backgroundimage', "purple");
		  	break;
		case '5':
		  	console.log("Grijs");
		  	req.io.broadcast('backgroundimage', "gray");
		  	break;
		case '6':
		  	console.log("Roze");
		  	req.io.broadcast('backgroundimage', "pink");
		  	break;
		case '7':
		  	console.log("Oranje");
		  	req.io.broadcast('backgroundimage', "orange");
		  	break;
		case '8':
		  	console.log("Donkerblauw");
		  	req.io.broadcast('backgroundimage', "darkblue");
		  	break;
		default:
		  	console.log("Fout");
		  	console.log(req.body.knop);
	}
});

app.get('/knop', routes.knop);

app.listen(3000)
